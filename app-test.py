# test_simple_module.py
from app import add_numbers

def test_add_numbers():
    assert add_numbers(2, 3) == 5
    assert add_numbers(-1, 1) == 0
    assert add_numbers(0, 0) == 0
    assert add_numbers(5, -3) == 2
    assert add_numbers(2,9) == 11
    assert add_numbers(2,11) == 13


